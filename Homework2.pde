/*
 * Artist: Sean 'HyDrone' Castillo
 * VIS 145A --- 2 February 2016
 */

//Animations
//Text; next time, make objects.
PImage seanOfApprovalText;                  //SEAN OF APPROVAL
int seanOfApprovalTextMovementCounter = 0;
boolean seanOfApprovalMovementSwitch = true;
int seanOfApprovalMover = 0;
int seanOfApprovalTextSpeed = 10;

PImage spaceWagonText;                      //THE SPACE WAGON IS COMING.
int spaceWagonTextMovementCounter = 0;      
int spaceWagonTextSpeed = 7;
int spaceWagonMover = 0;
boolean spaceWagonMovementSwitch = true;

PImage montyOumText;                        //KEEP MOVING FORWARD,
int montyOumTextMovementCounter = 0;
int montyOumTextSpeed = 11;
int montyOumMover = 0;
boolean montyOumMovementSwitch = true;

PImage tooLateText;                         //BEFORE IT'S TOO LATE.
int tooLateTextMovementCounter = 0;
int tooLateTextSpeed = 8;
int tooLateMover = 0;
boolean tooLateMovementSwitch = true;

PImage boxes1;
//variables to bob up and down
int boxes1MovementCounter = 0;
int boxes1Speed = 0;
int boxes1Mover = 0;
boolean boxes1MovementSwitch = true;


//Black Frame
PImage[] blackFrame = new PImage[6];
     int currentBlackFrameFrame = 0;
//Arrows
PImage[] arrows = new PImage[9];
     int currentArrowsFrame = 0;
//Plane
PImage[] plane = new PImage[13];
      int currentPlaneFrame = 0;
     
//Block 1
PImage[] block1 = new PImage[12];
      int currentBlock1Frame = 0;
      
//Wall-climbing animation
PImage[] bottom = new PImage[60];
      int currentBottomFrame = 0;
      
//Boxes Animation
PImage[] boxesAnim = new PImage[6];
      int currentBoxesAnimFrame = 0;
     
//Tentacle Animation
//PImage[] tentacleAnim = new PImage[36]; //version 4
PImage[] tentacleAnim = new PImage[33]; //version 5
      int currentTentacleAnimFrame = 0;

//Background Color Change
int[] vb = {163, 5, 198}; //violet background
int[] gb = {40, 198, 5};  //green background
int[] ob = {198, 85, 5}; //orange background
int colorCounter = 0;

void setup()
{
   size(768, 672); //the SNES resolution * 3
   frameRate(24);
   getAnimations();
   getOtherElements();
}

void getOtherElements()
{
   boxes1 = loadImage("boxes1.png");
  
   //Text
   seanOfApprovalText = loadImage("Text_seanOfApproval.png");
   spaceWagonText = loadImage("Text_spaceWagon.png");
   montyOumText = loadImage("Text_montyOum.png");
   tooLateText = loadImage("Text_tooLate.png");
}

void getAnimations()
{
   //blackFrame Animation
   for (int i=0; i<6; i++)
   {
      String blackFrameFrame = "blackFrame_frame" + nf(i,1) + ".png";
      blackFrame[i] = loadImage(blackFrameFrame);
   }
   
   //block 1 animation
   for (int i=0; i<12; i++)
   {
      String block1Frame = "block1_frame" + nf(i,1) + ".png";
      block1[i] = loadImage(block1Frame);
   }
   
   //Arrows Animation
   for (int i=0; i<9; i++)
   {
      String arrowsFrame = "Arrows_frame" + nf(i,1) + ".png";
      arrows[i] = loadImage(arrowsFrame);
   }
   
   //Plane Animation
   for (int i=0; i<13; i++)
   {
     String planeFrame = "planeFrame" + nf(i,1) + ".png";
     plane[i] = loadImage(planeFrame);
   }
   
   //Bottom (Wall-climb) Animation
   for (int i=0; i<33; i++)
   {
     String bottomFrame = "bottomFrame" + nf(i,1) + ".png";
     bottom[i] = loadImage(bottomFrame);
   }
   for (int i=33; i<60; i++) //for "resting" frames
   {
     bottom[i] = loadImage("bottomFrame0.png");
   }
   
   //Boxes Animation
   for (int i=0; i<6; i++)
   {
      String boxesFrame = "boxes_frame" + nf(i, 1) + ".png";
      boxesAnim[i] = loadImage(boxesFrame);
   }
   
   //Tentacle Animation
   for (int i=0; i<33; i++)
   {
      String tentacleFrame = "testTentV5Frame" + nf(i,1) + ".png";
      tentacleAnim[i] = loadImage(tentacleFrame);
   }
}

void displayBackground()
{
  colorCounter++;
  if (colorCounter <= 30)
  {
     background(vb[0], vb[1], vb[2]);
  }
  else if (colorCounter <= 60)
  {
     background(gb[0], gb[1], gb[2]);
  }
  else if (colorCounter <= 90)
  {
     background(ob[0], ob[1], ob[2]); 
  }
  else
  {
     colorCounter = 0; 
  }
}


//method to provide coordination;
//takes x movement, y movement, and object-specific movement counter as inputs
int[] moveAround(int xSpeed, int xMax, int ySpeed, int yMax, int movementCounter, boolean movementSwitch)
{
  int moveArray[] = {0, 0, 0}; // 0=x, 1=y, 2=movementCounter
  movementCounter++;
  
  if (movementCounter >= xMax && movementCounter >= yMax)
  {
    movementSwitch = !movementSwitch;
    movementCounter = 0;
  }
  if (movementSwitch == true)
  {
     moveArray[0] += xSpeed;
     moveArray[1] += ySpeed;
  }
  else
  {
     moveArray[0] -= xSpeed;
     moveArray[1] -= ySpeed;
  }
  moveArray[2] = movementCounter;
  return moveArray;
}

void draw()
{ 
  //Cycles background color
  displayBackground();
  
  //===Text Animations===
  
  //SEAN OF APPROVAL
  int[] seanOfApprovalMoveArray = moveAround(0, 0, 1, 500, seanOfApprovalTextMovementCounter, seanOfApprovalMovementSwitch);
  seanOfApprovalTextMovementCounter = seanOfApprovalMoveArray[2];
  seanOfApprovalMover=seanOfApprovalMover+seanOfApprovalTextSpeed;
  image(seanOfApprovalText, 40, -2000+seanOfApprovalMover);
  if (seanOfApprovalMover >= 2600)
  {
     seanOfApprovalMover = 0; 
  }
  
  //THE SPACE WAGON IS COMING.
  int[] spaceWagonMoveArray = moveAround(0, 0, 1, 500, spaceWagonTextMovementCounter, spaceWagonMovementSwitch);
  spaceWagonTextMovementCounter = spaceWagonMoveArray[2];
  spaceWagonMover=spaceWagonMover+spaceWagonTextSpeed;
  image(spaceWagonText, 220, 450-spaceWagonMover);
  if (spaceWagonMover >= 1000)
  {
     spaceWagonMover = 0; 
  }
  
  //KEEP MOVING FORWARD,
  int[] montyOumMoveArray = moveAround(10, 20, 0, 0, montyOumTextMovementCounter, montyOumMovementSwitch);
  montyOumTextMovementCounter = montyOumMoveArray[2];
  montyOumMover=montyOumMover+montyOumTextSpeed;
  image(montyOumText, -650+montyOumMover, 60);
  if (montyOumMover >= 1500)
  {
     montyOumMover = 0; 
  }
  
  //BEFORE IT'S TOO LATE.
  int[] tooLateMoveArray = moveAround(10, 20, 0, 0, tooLateTextMovementCounter, tooLateMovementSwitch);
  tooLateTextMovementCounter = tooLateMoveArray[2];
  tooLateMover=tooLateMover+tooLateTextSpeed;
  image(tooLateText, -700+tooLateMover, 30);
  if (tooLateMover >= 1500)
  {
     tooLateMover = 0; 
  }
  
  //===Other Animations===
  
  //black frame animation
  currentBlackFrameFrame = (currentBlackFrameFrame+1) % blackFrame.length;
  image(blackFrame[currentBlackFrameFrame], 0, 0);
  
  //block 1 animation
  currentBlock1Frame = (currentBlock1Frame+1) % block1.length;
  image(block1[currentBlock1Frame], 25, 25);
  
  //arrows animation
  currentArrowsFrame = (currentArrowsFrame+1) % arrows.length;
  image(arrows[currentArrowsFrame], 0, 0);
  
  //plane animation
  currentPlaneFrame = (currentPlaneFrame+1) % plane.length;
  image(plane[currentPlaneFrame], 0, 200);
  
  //boxes1 animation
  boxes1MovementCounter++;
  if (boxes1MovementCounter == 10)
  {
     boxes1MovementSwitch = !boxes1MovementSwitch;
     boxes1MovementCounter = 0;
  }
  if (boxes1MovementSwitch == true) {boxes1Mover--;}
  else {boxes1Mover++;}
  image(boxes1, 0, boxes1Mover+80);
  
  //bottom (Wall-climb) animation
  currentBottomFrame = (currentBottomFrame+1) % bottom.length;
  image(bottom[currentBottomFrame], 0, 0);
  
  //Boxes Animation
  currentBoxesAnimFrame = (currentBoxesAnimFrame+1) % boxesAnim.length;
  
  //Tentacle Animation
  currentTentacleAnimFrame = (currentTentacleAnimFrame+1) % tentacleAnim.length;
  image(tentacleAnim[currentTentacleAnimFrame], 0, -80);
}